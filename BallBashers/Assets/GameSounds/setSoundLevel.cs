﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class setSoundLevel : MonoBehaviour {
	
	public AudioMixer masterMixer;	
	
	private bool isMuted;
	private float masSave;
	
	private void start(){
		masSave = 0f;
	}
	public void setEffects(float effVol){
        if (PlayerPrefs.GetString("Overide") == "Yes")
        {

        }
        else
        {
            masterMixer.SetFloat("EffectsVolume", effVol);
        }
	}
	
	public void setMusic(float musicVol){
        if (PlayerPrefs.GetString("Overide") == "Yes")
        {

        }
        else
        {
            masterMixer.SetFloat("MusicVolume", musicVol);
        }
	}
	
	public void setMaster(float masVol){
        if (PlayerPrefs.GetString("Overide") == "Yes")
        {

        }
        else
        {
            masterMixer.SetFloat("MasterVolume", masVol);
        }
	}

    public void mute()
    {
        if (PlayerPrefs.GetString("Overide") == "Yes")
        {

        }
        else
        {
            isMuted = !isMuted;
            if (isMuted)
            {
                masterMixer.GetFloat("MasterVolume", out masSave);
                masterMixer.SetFloat("MasterVolume", -80f);
            }
            else
            {
                masterMixer.SetFloat("MasterVolume", masSave);
            }
        }
    }
}
