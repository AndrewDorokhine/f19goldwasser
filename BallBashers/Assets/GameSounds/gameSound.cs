﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameSound : MonoBehaviour {
	public AudioSource wallHit;
	public AudioSource foodEat;
	public AudioSource playerHit;
	
	//public List<GameObject> wallList = new List<GameObject>();
	public List<GameObject> playerList = new List<GameObject>();
	
	private void OnCollisionEnter(Collision col) {
		
		foreach(GameObject obj in playerList){
			if(col.gameObject == obj){
				playerHit.Play();
				return;
					
			}
			
		}
		if(col.gameObject.name != "Ground"){
			wallHit.Play();	
		}
		

    }
	
    private void OnTriggerEnter(Collider collision) {
        if (collision.gameObject.name == "TestFood")
        {

          	foodEat.Play();
            
        }
  	

    }	

}
