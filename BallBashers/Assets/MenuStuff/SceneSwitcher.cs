﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Summary: 
 * This script handles switcing between scenes. To use this script: attach it to an empty game object, 
 * link that object to the click event, and select the scene switch you desire.
 * 
 * 
 * Author: Patrick Martel (PM)
 */

 /*Scene Directory
  *  MainMenu - 0
  *  GameScene - 1
  *  TutorialTemp-2
  *  ScoresPage - 3
  *  SettingsPage - 4
  *  NullScene (Not being used) - 5
  *  AdminScene - 6
  *  CaseyGameScene - 7
  *  GameScenePatrick - 8
  */
public class SceneSwitcher : MonoBehaviour {
	
	//This goes to the test scene Andrew created, this will eventually be for the main game
	public void GoToMainScene() {
		
		SceneManager.LoadScene(1);
	}
	
	//This goes to the menu scene
	public void GoToMainMenu() {
		
		SceneManager.LoadScene(0);
	}

    public void GoToAdmin() {
        SceneManager.LoadScene(6);

    }
	
	//This goes to the tutorial test scene
	public void GoToTutorial() {
		
		SceneManager.LoadScene(2);
	}
	
	//This goes to the score scene
	public void GoToScore() {
		
		SceneManager.LoadScene(3);
	}
	
	
	//This goes to the setings scene
	public void GoToSettings() {
		
		SceneManager.LoadScene(4);
	}
}
