﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playMenuSound : MonoBehaviour
{
	public AudioSource buttonPress;
	
	public void playButtonPress(){
		buttonPress.Play();
	}
}
