﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class redirectPlayer : MonoBehaviour {
	public float speed = 10;
 	private void OnTriggerEnter(Collider collision) {
    	if (collision.gameObject.name == "Redirect") {

			GetComponent<Rigidbody>().AddForce(Random.Range(0, 10) * speed, 0,Random.Range(0, 10) * speed,ForceMode.Impulse);
        }
    }
}
