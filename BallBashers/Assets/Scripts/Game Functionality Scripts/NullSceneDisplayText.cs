﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NullSceneDisplayText : MonoBehaviour
{
    [SerializeField] Text _finalScoreText;
    // Start is called before the first frame update

    public Text enterName;

    void Start()
    {
        _finalScoreText.text = "You scored: " + PlayerPrefs.GetFloat("totalScore");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

