﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine.UI;

public class HighScoreManager : MonoBehaviour
{

    private string connectionString;

    private List<HighScore> highScores = new List<HighScore>();

    public GameObject scorePrefab;

    public Transform scoreParent;

    public int topRanks;

    public string newScoreName;


    // Start is called before the first frame update
    void Start()
    {
        connectionString = "URI=file:" + Application.dataPath + "/HighScoreDB.sqlite";
        DeleteOldScores();
        //newScoreName = PlayerPrefs.GetString("Name");
        //if (newScoreName != string.Empty)
        //{
        //   InsertScore(newScoreName, (int)PlayerPrefs.GetFloat("totalScore"));
        //    PlayerPrefs.SetString("Name", string.Empty);
        //}
        ShowScores();
    }

    // Update is called once per frame
    void Update()
    {

    }


 
    private void InsertScore(string name, int newScore)
    {
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                string sqlQuery = "SELECT * FROM BadNames WHERE name = '" + newScoreName + "'";

                dbCmd.CommandText = sqlQuery;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        
                    }
                    else
                    {
                        using (IDbCommand dbCmd2 = dbConnection.CreateCommand())
                        {
                            sqlQuery = String.Format("INSERT INTO HighScores(Name,Score) VALUES(\"{0}\",\"{1}\")", name, newScore);

                            dbCmd2.CommandText = sqlQuery;
                            dbCmd2.ExecuteScalar();
                        }
                    }
                    dbConnection.Close();
                    reader.Close();
                }
            }
        }
    }

    private void DeleteScore(int name)
    {
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                string sqlQuery = String.Format("DELETE FROM HighScores WHERE Name =\"{0}\"", name);

                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
            }

            dbConnection.Close();
        }
    }

    private void DeleteOldScores()
    {
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            GetScores();
            for (int i = 0; i < highScores.Count; i++)
            {
                if (highScores[i].Date.Ticks < DateTime.Now.Ticks - (28 * TimeSpan.TicksPerDay))
                {
                    using (IDbCommand dbCmd = dbConnection.CreateCommand())
                    {
                        string sqlQuery = String.Format("DELETE FROM HighScores WHERE PlayerID =\"{0}\"", highScores[i].ID);

                        dbCmd.CommandText = sqlQuery;
                        dbCmd.ExecuteScalar();
                    }
                }
               
            }
            dbConnection.Close();
        }
    }

    private void GetScores()
    {
        highScores.Clear();

        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                string sqlQuery = "SELECT * FROM HighScores";

                dbCmd.CommandText = sqlQuery;
                

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        highScores.Add(new HighScore(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2), reader.GetDateTime(3)));

                    }

                    dbConnection.Close();
                    reader.Close();
                }
            }
        }

        highScores.Sort();
    }

    private void ShowScores()
    {
        GetScores();
        topRanks = 8;
         
        for (int i = 0; i< topRanks; i++)
        {
            if (i <= highScores.Count -1)
            {
                GameObject tmpObjec = Instantiate(scorePrefab);

                HighScore tmpScore = highScores[i];

                tmpObjec.GetComponent<HighScoreScript>().SetScore(tmpScore.Name, tmpScore.Score.ToString(), "#" + (i + 1).ToString());

                tmpObjec.transform.SetParent(scoreParent);

                tmpObjec.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            }
        }

        
    }
}