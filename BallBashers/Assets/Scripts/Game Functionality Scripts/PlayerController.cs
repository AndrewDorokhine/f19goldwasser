﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler
{
    Rigidbody rb;
    Vector2 initialPressPosition;
    [SerializeField] private int _forceMultiplier;
    float _touchStarted;
    float _touchReleased;
    float camRayLength = 100f;
    Vector3 _startPos;
    [SerializeField] private GameEvent _resetTimeOutTrackerEvent;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        _startPos = gameObject.transform.position;
    }

    public void ResetPuckPosition()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        gameObject.transform.position = _startPos;
    }

    void Turning()
    {
        // Create a ray from the mouse cursor on screen in the direction of the camera.
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Create a RaycastHit variable to store information about what was hit by the ray.
        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit))
        {
            // Create a vector from the player to the point on the floor the raycast from the mouse hit.
            Vector3 playerToMouse = floorHit.point - transform.position;

            // Ensure the vector is entirely along the floor plane.
            playerToMouse.y = 0f;

            // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            // Set the player's rotation to this new rotation.
            //playerRigidbody.MoveRotation(newRotation);
            transform.eulerAngles = newRotation.eulerAngles;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        gameObject.transform.Rotate(0, 0, 0);
        initialPressPosition = new Vector2(eventData.position.x, eventData.position.y);
        _touchStarted = Time.time;

        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

    public void OnDrag(PointerEventData eventData)
    {
        _resetTimeOutTrackerEvent.Raise();
        Turning();
        gameObject.transform.position = new Vector3(eventData.pointerCurrentRaycast.worldPosition.x, gameObject.transform.position.y, eventData.pointerCurrentRaycast.worldPosition.z);
        //rb.AddForce(new Vector2(eventData.delta.x * _forceMultiplier, eventData.delta.y * _forceMultiplier));
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _touchReleased = Time.time;
        rb.AddForce(transform.forward * _forceMultiplier, ForceMode.Impulse);
        //float timeHeld = _touchReleased - _touchStarted;
        //Vector2 direction = eventData.position - initialPressPosition;
        //rb.AddForce((direction / timeHeld) * _forceMultiplier);
        //rb.AddForce((eventData.position.x - initialPressPosition.x) * _forceMultiplier, 0, (eventData.position.y - initialPressPosition.y)* _forceMultiplier);
    }


}
