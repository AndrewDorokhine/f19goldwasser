﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEndScript : MonoBehaviour
{
    public GrowShape puck1;
    public GrowShape puck2;
    public GrowShape puck3;
    public GrowShape puck4;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnGameEnd()
    {
        int score = puck1.score + puck2.score + puck3.score + puck4.score;
        PlayerPrefs.SetFloat("playerScore", score);
        SceneManager.LoadScene(5);
    }
}
