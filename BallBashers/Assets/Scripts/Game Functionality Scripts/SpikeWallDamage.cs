﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeWallDamage : PlayerBump
{
    
    public GrowShape playerScore3;


    private void OnCollisionEnter(Collision collision)

    {
        
        if (collision.gameObject.name == playerScore0.name)
        {
            
            SmallShape(playerScore0);
        }
        else if (collision.gameObject.name == playerScore1.name)
        {
            
            SmallShape(playerScore1);
        }
        else if (collision.gameObject.name == playerScore2.name)
        {
            
            SmallShape(playerScore2);
        }
        else if (collision.gameObject.name == playerScore3.name)
        {
            
            SmallShape(playerScore3);
        }
    }
}
