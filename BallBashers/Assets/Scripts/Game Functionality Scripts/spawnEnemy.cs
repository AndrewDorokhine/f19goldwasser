﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This is a script that spwan enemies in a designated area when a plyer walks over a particular object, for the purpose of this game we can use floor segments. 
 * This is a modified script by Brian Brookwell, TagBehavior, that was intended to spawn children playing tag. Now it spawns enemies thirsty for blood... 
 * 
 * public int enemyCount ...how many enemies we spawn in
 * 
 * public float spawnRadius ...how far from the middle of the designated area the enmies spawn
 *  
 * public GameObject spawnArea ...the area you want the enemies to spawn in. Use an empty game object so you can move it around freely. 
 * 
 * public GameObject player ...the player, this is *neccesary* or else the AI script won't run! 
 * 
 * public GameObject enemyPrefab ...the prefab we have for the enemy
 * 
 * private bool entered ...this tracks if the area has already been triggered so we don't spawn enemies every time a tile is walked over, that would be messy...
 * 
 * Author Brian Brookwell (BRB); modified by Patrick Martel (PM)
 */
public class spawnEnemy : MonoBehaviour {
	
	public int enemyCount = 4;
	
	public float spawnRadius = 10f;
	
	public GameObject spawnArea;
	
	public GameObject player;
		
	public GameObject enemyPrefab;
	
	private bool entered = false; //we don't want this to keep triggering
	
	
	// Use this for initialization
	void Start () {
		// end early if the neccesary components aren't present
		if (enemyPrefab == null) {
			Debug.Log("Please assign a enemy prefab.");
			return;
		}
		if(player == null){
			Debug.Log("Please assign a player prefab.");
			return;
		}
		if(spawnArea == null){
			Debug.Log("Please assign a spawn area.");
			return;
		}
	}
	
	/// <summary>
	/// This spawns enemies in the given area, it is called by OnCollisionEnter() 
	/// </summary>
	/// (BRB) Modified: 2018/12/14 (PM)
	void spawn() {
		// instantiate the enemies
		GameObject enemyTemp;
		TestEnemyAI db = null;

		for (int i = 0; i < enemyCount; i++) {
			enemyTemp = (GameObject) GameObject.Instantiate(enemyPrefab);
			enemyTemp.name = "TestEnemy";
			
			//assign the player to the enemies
			db = enemyTemp.GetComponent<TestEnemyAI>();
			db.player = player;
			
			// spawn inside circle
			Vector2 pos = new Vector2(spawnArea.transform.position.x, spawnArea.transform.position.z) + Random.insideUnitCircle * spawnRadius;
			enemyTemp.transform.position = new Vector3(pos.x, spawnArea.transform.position.y, pos.y);
			

		}

	}
	
	/// <summary>
	/// This detects if the player walks over the tile and triggers the spawn
	/// </summary>
	/// <param name="col">< a collision>
	/// 2018/12/14 (PM)
    void OnCollisionEnter (Collision col) {
        if(col.gameObject == player && entered == false) {
			spawn();
			entered = true;
        }

    }
}
