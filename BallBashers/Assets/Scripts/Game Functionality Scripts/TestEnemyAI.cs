﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Summary:
 * This script handles the behavoir of basic enemies in the game, they will wander around until the player nears them, if they are n a group of less than 3 they will flee,
 * otherwise they will move to attack the player. Their color indicates what they are doing, green = wandering, bule = fleeing, red = attacking. Once they begin attacking
 * they will not break off pursuit even if they leave thir allies behind.
 *  
 * 
 * Variables: 
 * public float speed ...the speed of the enemies
 * public GameObject player ...the player game object
 * public string allies ...the name of allied enemies **THIS IS IMPORTANT** the script needs to know what the other enemies are called to behave properly
 *
 * private int numAllies ...counts the number of nearby allies
 *
 * private float range ...the distance between the player and the enemy
 * private float radiusLookForAllies ...how far do we look arounf for allies
 * private float attackSpeed ...how fast to move when attacking
 * private float fleeSpeed ...how fast to move when fleeing
 * private float minDist ...how far the player needs to be before either attacking or fleeing 
 * private float timeToChangeDirection ...time inbetween changing directiond when wandering
 *
 * private bool alliesAttacking ...are nearby allies attacking the player
 * private bool attacking ...are we attacking 
 *	
 * Author: Patrick Martel (PM)
 */

public class TestEnemyAI : MonoBehaviour {
	
	public float speed = 5f;
	public GameObject player = null;
	public string allies = "TestEnemy";	
	
	private int numAllies;
	
	private float range;
	private float radiusLookForAllies = 1f;
	private float attackSpeed = 1f;
	private float fleeSpeed = 3f;
	private float minDist = 3f; //if this isn't set to private it doesn't work
	private float timeToChangeDirection; //also needs to be set to private
	
	private bool alliesAttacking;
	private bool attacking;
	
	private bool bounceBack = false;
	
	private Vector3 playerPos;
	
	
	// Use this for initialization
	void Start () {
		ChangeDirection();
		numAllies = 0; 
		playerPos = player.transform.position;
		range = 10f;
		alliesAttacking = false;
		attacking = false;
		bounceBack = false;
	}
	
	// Update is called once per frame
	void Update () {
		numAllies = 0;
		playerPos = player.transform.position;
		range = Vector3.Distance(transform.position, playerPos);
      	timeToChangeDirection -= Time.deltaTime;
      	numAllies = countAllies();
      	alliesAttacking = checkNearbyAlliesAttacking(); //this can cause a chain reaction in which all enmies become hostile
	}
	void FixedUpdate() {
      	
		//if a nearby ally is attacking join them
      	if(alliesAttacking) {
      		 attackPlayer();
      		 attacking = true;
      	}
      	
		//once you've started attacking keep doing it
      	if(attacking && !bounceBack) {
      		attackPlayer();
      	}
      	
		//the next two if statements need the !attacking otherwise they flicker between states, while trippy to look at its not ideal behavoir. 
		
		//handles the random movement
      	if( range > minDist && !attacking) {
      		
	         if (timeToChangeDirection <= 0) {
            	ChangeDirection();
         	}
      	}
      	
		//handles the decision to flee or attack
      	if(range < minDist && !attacking) {
      		if(numAllies < 3) {
      			flee();
      		} else {
      			
      			attackPlayer();
      			attacking = true;
      		 }	
  		}
				
		
	}
	
	/// <summary>
	/// This is used to turn away from walls, ideally preventing the enemies from getting stuck on walls and corners
	/// </summary>
	/// 2018/12/14 PM
	private void turnFromWall() {
		float angle = 90f;
		if(Physics.Raycast(transform.position, Vector3.forward, 1)) {
		   	transform.Rotate( new Vector3(0, angle, 0), Space.Self );
		}
	}
	
	/// <summary>
	/// This makes the enemy wander around by picking a random direction and moving there
	/// </summary>
	/// Based on the scrip from here: https://answers.unity.com/questions/552674/make-a-character-walk-around-randomly.html
	/// 2018/12/14 PM
	 private void ChangeDirection() {
	    float angle = Random.Range(0f, 360f);
	    int dir = Random.Range(0, 3);
	    
	    transform.Rotate( new Vector3(0, angle, 0), Space.Self );
	    
	    turnFromWall();
	    
	    this.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
	    
	    if(dir == 1){
	    	GetComponent<Rigidbody>().velocity = transform.forward * 2;
	    }
	    if(dir == 2){
	    	GetComponent<Rigidbody>().velocity = transform.up * 2;
	    } else{    
	    	GetComponent<Rigidbody>().velocity = transform.right * 2;
	    }
	     timeToChangeDirection = 2.5f;
	     
	 }
	
	/// <summary>
	///  This makes the emeie move away from the player, fleeing
	/// </summary>
	/// 2018/12/14 PM
	private void flee() {
			this.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);	
			transform.position = Vector3.MoveTowards(transform.position, player.transform.position, -1 * fleeSpeed * Time.deltaTime);		
			turnFromWall();
	}
	
	
	/// <summary>
	///  This returns the number of nearby enemies, this helps them decide to attack or flee
	/// </summary>
	/// <returns nearAllies><an integer value fo the number of nearby allies>
	/// 2018/12/14 PM
	private int countAllies(){
		Collider[] colliders = Physics.OverlapSphere(transform.position, radiusLookForAllies);	
		int nearAllies = 0;
		
		foreach (Collider hit in colliders) {
			Collider col = hit.GetComponent<Collider>();
			if(col.gameObject.name.Equals(allies)) {
			 	nearAllies++;
 			}
		}
		return nearAllies;
	}
	
	/// <summary>
	/// This makes the enemies move to a rally point within their range. It's not used yet but I'm keeping it as it may prove useful in the future 
	/// </summary>
	/// 2018/12/14 PM
	private void moveToRallyPoint(){
		Collider[] colliders = Physics.OverlapSphere(transform.position, 20f);	
		
		foreach (Collider hit in colliders) {
			Collider col = hit.GetComponent<Collider>();
			if(col.gameObject.name.Equals("rallyPoint")) {
		 		transform.position = Vector3.MoveTowards(transform.position, col.transform.position, 1 * speed * Time.deltaTime);
		 		break;
 			}
		}
		
	}
	
	/// <summary>
	/// This is used to make the enemy attack the player, which involves running right up to them and touching them, how this touch is resolved is handled by another script
	/// </summary>
	/// 2018/12/14 PM
	private void attackPlayer() {
		//bool turnedToPlayer = Physics.Raycast(transform.position, Vector3.forward, 5);
		
		//if(!turnedToPlayer) {
		 //transform.Rotate( player.transform.position, Space.Self );
		//} //turn to the player
		
		transform.position = Vector3.MoveTowards(transform.position, player.transform.position, 1 * attackSpeed * Time.deltaTime);
		this.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
		turnFromWall();	
	}
	
	/// <summary>
	/// This is used to check if nearby allies ate attacking the player. This is used to help pull other enemies into the combat.
	/// </summary>
	/// <returns bool>< true or false>
	/// 2018/12/14 PM
	private bool checkNearbyAlliesAttacking() {
		Collider[] colliders = Physics.OverlapSphere(transform.position, 1f);
		
		foreach (Collider hit in colliders) {
		Collider col = hit.GetComponent<Collider>();
			if(col.gameObject.name.Equals(allies)) {
			 	if(col.GetComponent<TestEnemyAI>().attacking == true){
			 		return true;
			 	}
		 	
 			}
		}
		return false;
	}
	
	/// <summary>
	/// This makes an enemy die if they touch a bullet object
	/// </summary>
	/// <param name="col">< a collision>
	/// 2018/12/14 PM
    void OnCollisionEnter (Collision col) {
        if(col.gameObject.name == "Bullet") {
            Destroy(col.gameObject);
        }
        if(col.gameObject.name == "Player") {
			bounceBack = true;
            this.GetComponent<Rigidbody>().velocity = transform.forward * -10;
        }		
    }
}

