﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstLevelScoreReset : MonoBehaviour
{
    // Very simple script to reset the score to 0 at the start of a new game. 
    void Start()
    {
        PlayerPrefs.SetFloat("totalScore", 0);
    }
}
