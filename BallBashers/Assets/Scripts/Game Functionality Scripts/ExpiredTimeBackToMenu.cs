﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExpiredTimeBackToMenu : MonoBehaviour
{
    [SerializeField] private float _timeOutTimer;

    private float _timeRemainingToTimeOut;
    private bool _isTrackingTime;

    // Start is called before the first frame update
    void Start()
    {
        _isTrackingTime = true;
        DontDestroyOnLoad(this.gameObject);
        _timeRemainingToTimeOut = _timeOutTimer;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isTrackingTime)
        {
            _timeRemainingToTimeOut -= Time.deltaTime;
            Debug.Log(_timeRemainingToTimeOut);
            if (_timeRemainingToTimeOut <= 0)
            {
                GoToMainMenu();
                DestroyMenuResetter();
            }
        }
    }

    public void ResetCurrentTimer()
    {
        _timeRemainingToTimeOut = _timeOutTimer;
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void DestroyMenuResetter()
    {
        Destroy(gameObject);
    }
}
