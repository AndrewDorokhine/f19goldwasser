﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine.UI;
using UnityEngine.Audio;

public class AdminManager : MonoBehaviour
{
    private string connectionString;

    public InputField login;
    public string loginName;
    public InputField target;

    public string targetName;

    public GameObject inputField;
    public GameObject inputButton;
    public GameObject muteButton;

    public AudioMixer masterMixer;
    private bool isMuted;
    private float masSave;

    // Start is called before the first frame update
    void Start()
    {
        connectionString = "URI=file:" + Application.dataPath + "/HighScoreDB.sqlite";

        masSave = 0f;
    }


        public void CheckPassword()
    {
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                loginName = login.text;
                string sqlQuery = "SELECT * FROM Logins WHERE password = '" + loginName +"'";

                dbCmd.CommandText = sqlQuery;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        DisplayToggleOn();
                    }
                    else
                    {
                        // POP UP
                        Debug.Log("NOT FOUND: " + loginName);
                    }
                    dbConnection.Close();
                    reader.Close();
                }
            }
        }
    }

    public void volumeOverride()
    {
        
        isMuted = !isMuted;
        if (isMuted)
        {
            masterMixer.GetFloat("MasterVolume", out masSave);
            masterMixer.SetFloat("MasterVolume", -80f);
            PlayerPrefs.SetString("Override", "Yes");
        }
        else
        {
            masterMixer.SetFloat("MasterVolume", masSave);
            PlayerPrefs.SetString("Override", "No");
        }
    }

    public void DeleteName()
    {
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();

            targetName = target.text;
            target.text = string.Empty;

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                string sqlQuery = String.Format("DELETE FROM HighScores WHERE Name =\"{0}\"", targetName);
                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
                sqlQuery = String.Format("INSERT INTO BadNames (name) VALUES (\"{0}\")", targetName);
                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
            }

            dbConnection.Close();
        }
    }

    public void DisplayToggleOn()
    {
        if (inputField.activeInHierarchy == false)
        {
            inputField.SetActive(true);
            inputButton.SetActive(true);
            muteButton.SetActive(true);
        }
    }

    public void DisplayToggleOff()
    {
        if (inputField.activeInHierarchy == true)
        {
            inputField.SetActive(false);
            inputButton.SetActive(false);
            muteButton.SetActive(false);
        }
    }
}

