﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine.SceneManagement;

public class SaveName : MonoBehaviour
{
    private string connectionString;

    public InputField input;
    public GameObject warning;

    public string enterName;

    // Start is called before the first frame update
    void Start()
    {
        connectionString = "URI=file:" + Application.dataPath + "/HighScoreDB.sqlite";
        //input.text = connectionString;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InsertScore()
    {
        enterName = input.text;
        if (enterName != string.Empty)
        {
            using (IDbConnection dbConnection = new SqliteConnection(connectionString))
            {
                dbConnection.Open();
                using (IDbCommand dbCmd = dbConnection.CreateCommand())
                {
                    string sqlQuery = "SELECT * FROM BadNames WHERE name = '" + enterName + "'";

                    dbCmd.CommandText = sqlQuery;

                    using (IDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            warning.SetActive(true);
                            dbConnection.Close();
                            reader.Close();
                        }
                        else
                        {
                            using (IDbCommand dbCmd2 = dbConnection.CreateCommand())
                            {
                                sqlQuery = String.Format("INSERT INTO HighScores(Name,Score) VALUES(\"{0}\",\"{1}\")", enterName, (int)PlayerPrefs.GetFloat("totalScore"));

                                dbCmd2.CommandText = sqlQuery;
                                dbCmd2.ExecuteScalar();
                                dbConnection.Close();
                                reader.Close();
                                SceneManager.LoadScene(3);
                            }
                        }
                    }
                }
            }
        }
    }

    //public void RegisterName()
    //{
    //    enterName = input.text;
    //    if (enterName != string.Empty)
    //    {
    //        PlayerPrefs.SetString("Name", enterName);
    //    }
    //}
}
