﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBump : MonoBehaviour
{
    public GrowShape playerScore0;
    public GrowShape playerScore1;
    public GrowShape playerScore2;
    public float x;
    public float y;
    public float z;



    private void OnCollisionEnter(Collision collision)
    {


        if (collision.gameObject.name == playerScore0.name)
        {
            SmallShape(playerScore0);
        }
        else if (collision.gameObject.name == playerScore1.name)
        {
            SmallShape(playerScore1);
        }
        else if (collision.gameObject.name == playerScore2.name)
        {
            SmallShape(playerScore2);
        }
    }

    public void SmallShape(GrowShape objectname)
    {
        if (objectname.transform.localScale.z > 7 && objectname.transform.localScale.x > 7)
        {
            objectname.transform.localScale -= new Vector3(x, y, z);
        }
        
        if (objectname.score <= 0)
        {
            
            objectname.score = 0;
            objectname.scoreText.text = "0";
        }
        else if(objectname.score >= 0)
        {
            objectname.score -= 1;
            objectname.scoreText.text = objectname.score.ToString("F0");
        }
        
    }
}
