﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DemoButton : MonoBehaviour, IDragHandler
{
    [SerializeField] private string _messageToDisplay;

    // Start is called before the first frame update
    void Start()
    {
        //Empty as demo code
    }

    // Update is called once per frame
    void Update()
    {
        //Empty as demo code
    }

    public void DemoButtonClicked()
    {
        Debug.Log(_messageToDisplay);
    }

    public void OnDrag(PointerEventData eventData)
    {
        gameObject.transform.position = new Vector3(eventData.pointerCurrentRaycast.worldPosition.x, gameObject.transform.position.y, eventData.pointerCurrentRaycast.worldPosition.z);
    }
}
