﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimeTracker : MonoBehaviour
{
    [SerializeField] float _timeRemaining;
    [SerializeField] Image _timerImage;
    [SerializeField] GameEvent _gameEndEvent;
    [SerializeField] GameEvent _newLevelEvent;

    private float _originalTime;

    // Start is called before the first frame update
    void Start()
    {
        _newLevelEvent.Raise();
        _originalTime = _timeRemaining;
        _timerImage.fillAmount = _timeRemaining/_originalTime;
    }

    // Update is called once per frame
    void Update()
    {
        _timeRemaining -= Time.deltaTime;
        _timerImage.fillAmount = _timeRemaining/_originalTime;
        if(_timeRemaining <= 0)
        {
            _gameEndEvent.Raise();
        }
    }

    public void SwitchSceneToSecondLevel()
    {
        SceneManager.LoadScene(7);
    }

    public void SwitchSceneToThirdLevel()
    {
        SceneManager.LoadScene(8);
    }

    public void SwitchSceneToEndGame()
    {
        SceneManager.LoadScene(5);
    }
}
