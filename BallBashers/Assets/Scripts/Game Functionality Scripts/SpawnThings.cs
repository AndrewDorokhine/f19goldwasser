﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Summary:
 * This script will spawn a set ammount of food objects. 
 * After theat point the script counts the number of food objects around and determines of it should spawn more.
 * 
 * Variables:
 * public GameObject food ... This is the food object to spawn **IMPORTANT** if the food needs a script attached to it 
 * this script will need to be modified
 * public GameObject spawnArea ...The object around which the objects will spawn
 * 
 * public string foodName ...What the food objects will be called **IMPORTANT** the script re;ies on looking at object names 
 * to determine if they should spawn
 * 
 * public int minFood ..The ammount of food objects left before more are spawned
 * public int maxFoodSpawn ...The number of sfood to spawn
 * 
 * public float spawnRadius ...The radius around the spawn point where food will randomly spawn
 * public float lookRange ...How far around the spawn point to look for food objects to determine if more will spawn
 * 
 * private int nearFoodCount ...
 * 
 * Author: Parick Martel (PM)
 */
 
public class SpawnThings : MonoBehaviour
{
	
	public GameObject food;
	
	public GameObject spawnArea;
	
	public string foodName = "TestFood";
	
	public int minFood = 3;
	
	public int maxFoodSpawn = 10;
	
	public float spawnRadius = 20f;
	
	public float lookRange = 100f;
	
	private int nearFoodCount = 10;
	
	//Update that is called once per frame
    void FixedUpdate () {
    	
		//Count the food then decide if more should be spawned
		nearFoodCount = countFood();
    	
    	if (nearFoodCount <= minFood){
    		spawn();
    	}
    }
    
    /// <summary>
	///  This returns the number of nearby food objects
	/// </summary>
	/// <returns nearFood><an integer value fo the number of nearby food near the spawn point>
	/// 2018/12/14 PM
	public int countFood(){
		Collider[] colliders = Physics.OverlapSphere(transform.position, lookRange);	
		int nearFood = 0;
		
		foreach (Collider hit in colliders) {
			Collider col = hit.GetComponent<Collider>();
			if(col.gameObject.name.Equals(foodName)) {
			 	nearFood++;
 			}
		}
		return nearFood;
	}
   
	/// <summary>
	///  This creates food objects and drops them around the spawn area
	/// </summary>
    void spawn() {
		// instantiate the food
		GameObject foodTemp;
		
		for (int i = 0; i < maxFoodSpawn; i++) {
			foodTemp = 	(GameObject) GameObject.Instantiate(food, gameObject.transform);	
			foodTemp.name = foodName;
			
			// spawn inside circle
			Vector2 pos = new Vector2(spawnArea.transform.position.x, spawnArea.transform.position.z) + Random.insideUnitCircle * spawnRadius;
			foodTemp.transform.position = new Vector3(pos.x, spawnArea.transform.position.y, pos.y);			
		}

	}
    
    
}
