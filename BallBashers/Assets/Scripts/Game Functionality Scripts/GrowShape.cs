﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GrowShape : MonoBehaviour
{   
    /// <summary>
    /// Makes object disappear when coming in contact with a smaller object
    /// 
    /// objectName      Name of object being destroyed
    /// x   Change to the x axis of larger object
    /// y   Change to the y axis of larger object
    /// z   Change to the z axis of larger object
    /// </summary>
    public string objectName = "TestFood";
    public Text scoreText;
    public float x = 0.1f;
    public float y = 0.1f;
    public float z = 0;
    public int score;
    public string scoreName;
    public int scaleLock;
    
    
    //public AudioSource foodEat;
    //public AudioSource wallHit;

    /// <summary>
    /// Sets score to zero and initializes scoreText
    /// </summary>
    private void Awake()
    {
        score = 0;
        scoreText.text = score.ToString();
    }

    /// <summary>
    /// On collision, destroys food object and makes the shape bigger while increasing score and displays score on object
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name == objectName)
        {
            Destroy(collision.gameObject);
            if (this.transform.localScale.x < scaleLock && this.transform.localScale.z < scaleLock)
            {
                transform.localScale += new Vector3(x, y, z);
            }
            score += 1;
            scoreText.text = score.ToString("F0");
        }
       
    }

    public void OnGameEnd()
    {
        PlayerPrefs.SetFloat("totalScore", (PlayerPrefs.GetFloat("totalScore") + score));
        //SceneManager.LoadScene(5);
    }
}
