﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateAroundThing : MonoBehaviour
{
	 public float xAngle, yAngle, zAngle;
	 public GameObject obj; 

    // Update is called once per frame
    void Update()
    {
       this.transform.RotateAround(obj.transform.position, obj.transform.up, 10*Time.deltaTime); 
    }
}
