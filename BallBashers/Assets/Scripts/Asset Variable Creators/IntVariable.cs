﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Integer Variable", menuName = "Asset Variables/New Integer")]
public class IntVariable : ScriptableObject
{
    public int value;
}
