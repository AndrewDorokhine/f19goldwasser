﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Boolean Variable", menuName = "Asset Variables/New Boolean")]
public class BoolVariable : ScriptableObject
{
    public bool value;
}
