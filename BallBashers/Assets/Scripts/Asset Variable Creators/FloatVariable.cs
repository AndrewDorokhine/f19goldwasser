﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Float Variable", menuName = "Asset Variables/New Float")]
public class FloatVariable : ScriptableObject
{
    public float value;
}